\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{8}{section.1}
\contentsline {subsection}{\numberline {1.1}Contexto}{8}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Objetivo}{8}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Objetivos Espec\IeC {\'\i }ficos}{8}{subsubsection.1.2.1}
\contentsline {section}{\numberline {2}Revis\IeC {\~a}o Bibliogr\IeC {\'a}fica}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Teoria Musical}{10}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Sem\IeC {\'\i }nima}{10}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Harmonia}{10}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Melodia}{10}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Arranjo}{10}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}Piano Roll}{10}{subsubsection.2.1.5}
\contentsline {subsubsection}{\numberline {2.1.6}Partitura}{10}{subsubsection.2.1.6}
\contentsline {subsection}{\numberline {2.2}Gera\IeC {\c c}\IeC {\~a}o Procedural}{10}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Fractais}{12}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3}Linguagem Formal e Gram\IeC {\'a}tica Formal}{12}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Sistemas Lindenmayer}{13}{subsubsection.2.3.1}
\contentsline {paragraph}{\numberline {2.3.1.1}Livre-de-Contexto}{13}{paragraph.2.3.1.1}
\contentsline {paragraph}{\numberline {2.3.1.2}Sens\IeC {\'\i }vel-ao-Contexto}{13}{paragraph.2.3.1.2}
\contentsline {paragraph}{\numberline {2.3.1.3}L-System Determin\IeC {\'\i }stico}{14}{paragraph.2.3.1.3}
\contentsline {paragraph}{\numberline {2.3.1.4}L-System Estoc\IeC {\'a}stico}{14}{paragraph.2.3.1.4}
\contentsline {paragraph}{\numberline {2.3.1.5}Gr\IeC {\'a}fico de Tartaruga}{14}{paragraph.2.3.1.5}
\contentsline {subsection}{\numberline {2.4}Musical Instrument Digital Interface}{14}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Arquivo MIDI}{14}{subsubsection.2.4.1}
\contentsline {paragraph}{\numberline {2.4.1.1}Bloco de Cabe\IeC {\c c}alho}{15}{paragraph.2.4.1.1}
\contentsline {paragraph}{\numberline {2.4.1.2}Bloco de Faixa}{16}{paragraph.2.4.1.2}
\contentsline {subsection}{\numberline {2.5}Pulse-Code Modulation}{17}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}PortAudio}{18}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Simple and Fast Multimedia Library}{18}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Trabalhos Relacionados}{18}{subsection.2.8}
\contentsline {subsubsection}{\numberline {2.8.1}LMUSe}{18}{subsubsection.2.8.1}
\contentsline {subsubsection}{\numberline {2.8.2}lsys2midi}{18}{subsubsection.2.8.2}
\contentsline {section}{\numberline {3}Desenvolvimento}{19}{section.3}
\contentsline {subsection}{\numberline {3.1}Estudo de L-Systems}{19}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Algoritmo de Reprodu\IeC {\c c}\IeC {\~a}o}{19}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Algoritmo de Piano Roll}{20}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Avalia\IeC {\c c}\IeC {\~a}o Musical}{20}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Compara\IeC {\c c}\IeC {\~a}o com M\IeC {\'u}sicas}{20}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Compara\IeC {\c c}\IeC {\~a}o de Resultados}{20}{subsubsection.3.4.2}
\contentsline {subsection}{\numberline {3.5}Algoritmo de Altera\IeC {\c c}\IeC {\~a}o Musical}{20}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Avalia\IeC {\c c}\IeC {\~a}o do Algoritmo de Altera\IeC {\c c}\IeC {\~a}o Musical}{21}{subsubsection.3.5.1}
\contentsline {section}{\numberline {4}Conclus\IeC {\~o}es}{22}{section.4}
\contentsline {section}{\numberline {5}Refer\IeC {\^e}ncias}{23}{section.5}
