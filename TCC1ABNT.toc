\changetocdepth {4}
\select@language {brazil}
\select@language {english}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Lista de ilustra\c {c}\~{o}es}{4}{section*.4}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Lista de tabelas}{5}{section*.5}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Sum\'ario}{7}{section*.8}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Contexto}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivo geral}{9}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivos espec\IeC {\'\i }ficos}{10}{subsection.1.2.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Revis\IeC {\~a}o Bibliogr\IeC {\'a}fica}}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Fundamentos musicais}{11}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Notas musicais}{12}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Escalas}{12}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Escala maior natural}{12}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}Escala menor natural}{13}{subsubsection.2.1.2.2}
\contentsline {subsubsection}{\numberline {2.1.2.3}Escala menor harm\IeC {\^o}nica}{14}{subsubsection.2.1.2.3}
\contentsline {subsubsection}{\numberline {2.1.2.4}Escala menor mel\IeC {\'o}dica}{14}{subsubsection.2.1.2.4}
\contentsline {subsection}{\numberline {2.1.3}Melodia}{15}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Harmonia}{15}{subsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.4.1}Acordes}{15}{subsubsection.2.1.4.1}
\contentsline {subsection}{\numberline {2.1.5}Ritmo}{16}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Din\IeC {\^a}mica}{17}{subsection.2.1.6}
\contentsline {section}{\numberline {2.2}Gera\IeC {\c c}\IeC {\~a}o Procedural}{18}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Fractais}{20}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Linguagem Formal e Gram\IeC {\'a}tica Formal}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Sistemas Lindenmayer}{22}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}L-System determin\IeC {\'\i }stico}{23}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}L-System estoc\IeC {\'a}stico}{23}{subsubsection.2.3.1.2}
\contentsline {subsubsection}{\numberline {2.3.1.3}Gr\IeC {\'a}ficos tartaruga}{24}{subsubsection.2.3.1.3}
\contentsline {section}{\numberline {2.4}Musical Instrument Digital Interface}{25}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Arquivo MIDI}{25}{subsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.1.1}Bloco de cabe\IeC {\c c}alho}{26}{subsubsection.2.4.1.1}
\contentsline {subsubsection}{\numberline {2.4.1.2}Bloco de faixa}{27}{subsubsection.2.4.1.2}
\contentsline {section}{\numberline {2.5}Piano Roll}{28}{section.2.5}
\contentsline {section}{\numberline {2.6}Pulse-Code Modulation}{29}{section.2.6}
\contentsline {section}{\numberline {2.7}PortAudio}{30}{section.2.7}
\contentsline {section}{\numberline {2.8}Simple and Fast Multimedia Library}{30}{section.2.8}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Desenvolvimento}}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}Estudo de L-Systems}{31}{section.3.1}
\contentsline {section}{\numberline {3.2}Algoritmo de reprodu\IeC {\c c}\IeC {\~a}o}{31}{section.3.2}
\contentsline {section}{\numberline {3.3}Algoritmo de Piano Roll}{31}{section.3.3}
\contentsline {section}{\numberline {3.4}Avalia\IeC {\c c}\IeC {\~a}o musical}{32}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Compara\IeC {\c c}\IeC {\~a}o com m\IeC {\'u}sicas}{32}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Compara\IeC {\c c}\IeC {\~a}o de resultados}{32}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Algoritmo de altera\IeC {\c c}\IeC {\~a}o musical}{32}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Avalia\IeC {\c c}\IeC {\~a}o do algoritmo de altera\IeC {\c c}\IeC {\~a}o musical}{33}{subsection.3.5.1}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^{e}ncias bibliogr\IeC {\'a}ficas}}{34}{section*.10}
