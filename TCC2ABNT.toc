\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\select@language {english}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Lista de ilustra\c {c}\~{o}es}{5}{section*.6}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Lista de tabelas}{6}{section*.7}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Sum\'ario}{8}{section*.10}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{10}{chapter.1}
\contentsline {section}{\numberline {1.1}Contexto}{10}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivo geral}{11}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivos espec\IeC {\'\i }ficos}{11}{subsection.1.2.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Revis\IeC {\~a}o Bibliogr\IeC {\'a}fica}}{12}{chapter.2}
\contentsline {section}{\numberline {2.1}Fundamentos musicais}{12}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Notas musicais}{12}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Escalas}{13}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Escala maior natural}{13}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}Escala menor natural}{14}{subsubsection.2.1.2.2}
\contentsline {subsubsection}{\numberline {2.1.2.3}Escala menor harm\IeC {\^o}nica}{15}{subsubsection.2.1.2.3}
\contentsline {subsubsection}{\numberline {2.1.2.4}Escala menor mel\IeC {\'o}dica}{15}{subsubsection.2.1.2.4}
\contentsline {subsection}{\numberline {2.1.3}Melodia}{16}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Harmonia}{16}{subsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.4.1}Acordes}{16}{subsubsection.2.1.4.1}
\contentsline {subsection}{\numberline {2.1.5}Ritmo}{18}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Din\IeC {\^a}mica}{19}{subsection.2.1.6}
\contentsline {section}{\numberline {2.2}Gera\IeC {\c c}\IeC {\~a}o procedural}{19}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Fractais}{21}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Linguagem formal e gram\IeC {\'a}tica formal}{22}{section.2.3}
\contentsline {section}{\numberline {2.4}Sistemas Lindenmayer}{23}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}L-System determin\IeC {\'\i }stico}{24}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}L-System estoc\IeC {\'a}stico}{24}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Gr\IeC {\'a}ficos tartaruga}{25}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Musical Instrument Digital Interface}{26}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Arquivo MIDI}{26}{subsection.2.5.1}
\contentsline {subsubsection}{\numberline {2.5.1.1}Bloco de cabe\IeC {\c c}alho}{27}{subsubsection.2.5.1.1}
\contentsline {subsubsection}{\numberline {2.5.1.2}Bloco de faixa}{29}{subsubsection.2.5.1.2}
\contentsline {section}{\numberline {2.6}Pulse-Code Modulation}{30}{section.2.6}
\contentsline {section}{\numberline {2.7}PortAudio}{31}{section.2.7}
\contentsline {section}{\numberline {2.8}Trabalhos relacionados}{31}{section.2.8}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Desenvolvimento}}{32}{chapter.3}
\contentsline {section}{\numberline {3.1}Proposta de composi\IeC {\c c}\IeC {\~a}o musical}{32}{section.3.1}
\contentsline {section}{\numberline {3.2}Materiais}{33}{section.3.2}
\contentsline {section}{\numberline {3.3}Metodologia}{33}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Algoritmos de L-Systems}{33}{subsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.1.1}Cria\IeC {\c c}\IeC {\~a}o de L-Systems}{33}{subsubsection.3.3.1.1}
\contentsline {subsubsection}{\numberline {3.3.1.2}Cria\IeC {\c c}\IeC {\~a}o de acordes musicais}{35}{subsubsection.3.3.1.2}
\contentsline {subsubsection}{\numberline {3.3.1.3}Gera\IeC {\c c}\IeC {\~a}o de ritmo musical}{35}{subsubsection.3.3.1.3}
\contentsline {subsection}{\numberline {3.3.2}Algoritmo de cria\IeC {\c c}\IeC {\~a}o do arquivo MIDI}{36}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Algoritmo de reprodu\IeC {\c c}\IeC {\~a}o em tempo de execu\IeC {\c c}\IeC {\~a}o}{37}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Avalia\IeC {\c c}\IeC {\~a}o musical}{40}{subsection.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.4.1}Pesquisa com interessados em m\IeC {\'u}sicas de jogos digitais}{40}{subsubsection.3.3.4.1}
\contentsline {subsubsection}{\numberline {3.3.4.2}Avalia\IeC {\c c}\IeC {\~a}o de m\IeC {\'u}sicos que compuseram m\IeC {\'u}sicas para jogos digitais}{41}{subsubsection.3.3.4.2}
\contentsline {section}{\numberline {3.4}Resultados}{42}{section.3.4}
\contentsline {section}{\numberline {3.5}An\IeC {\'a}lise dos resultados}{44}{section.3.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Conclus\IeC {\~a}o}}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}Trabalhos futuros}{45}{section.4.1}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^{e}ncias bibliogr\IeC {\'a}ficas}}{47}{section*.12}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{49}{section*.13}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Arquivos MIDI resultantes}}{50}{appendix.A}
